﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private InputManager inputManager;
    private HUD hud;
    private Player Player;
   

    public bool isPaused;
    public bool gameover;
    public bool startGame = false;
    private float timeCounter;

    void Start()
    {
        isPaused = false;
        startGame = false;

        inputManager = new InputManager();
        inputManager.Initialize();

        hud = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUD>();
        hud.ClosePausePanel();
        hud.CloseGameoverPanel();
        hud.CloseGameplayPanel();
        hud.OpenTitlePanel();

        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        Player.Initialize();
        

        timeCounter = 0;
        hud.UpdateTimeText(timeCounter);


    }
    void Update()
    {
        if (!startGame) return;
        if (gameover)
        {
            Player.DeadUpdate();
            return;
        }

        inputManager.ReadInput(); //Update Input

        Player.MyUpdate(); //Update kirby
        

        timeCounter += Time.deltaTime;
        hud.UpdateTimeText(timeCounter);
    }
    public void PauseGame()
    {
        isPaused = !isPaused;

        if (isPaused)
        {
            Time.timeScale = 0;
            hud.OpenPausePanel();
        }
        else
        {
            Time.timeScale = 1;
            hud.ClosePausePanel();
        }
    }
    public void StartGame()
    {
        startGame = true;
        hud.CloseTitlePanel();
        hud.OpenGameplayPanel();
    }
    public void GameOver()
    {
        gameover = true;

        SaveGame();

        hud.UpdateGameOverPanel();
        hud.OpenGameoverPanel();
    }
    void SaveGame()
    {
        float record = 0;

        if (PlayerPrefs.HasKey("Record"))
        {
            record = PlayerPrefs.GetFloat("Record");
            if (timeCounter > record) record = timeCounter;
        }
        else record = timeCounter;

        PlayerPrefs.SetFloat("Record", record);
        PlayerPrefs.SetFloat("Score", timeCounter);
    }
    public void LoadScene(int num)
    {
        SceneManager.LoadScene(num);
    }
}