﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    private Rigidbody2D myRigidBody;

    private Animator myAnimator;

    private bool isDead;

    private Animator anim;

    private GameManager gameManager;

    private HUD hud;
    

    [SerializeField]
    private float movementspeed;

    private bool Attack;

    private bool slide;

    private bool facingRight;

    [SerializeField]

    private Transform[] groundPoints;

    [SerializeField]

    private float groundRadius;

    [SerializeField]
    private LayerMask whatIsGround;

    private bool isGrounded;

    private bool jump;

   

    [SerializeField]
    private float jumpForce;

    [SerializeField]
    private bool airControl;

    // Use this for initialization
    void Start()
    {
        facingRight = true;
        myRigidBody = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
        anim = GetComponentInChildren<Animator>();

    }
    void Update()
    {
        HandleInput();
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        if (isDead == false)
        {
            float horizontal = Input.GetAxis("Horizontal");

            isGrounded = IsGrounded();

            HandleMovement(horizontal);

            Flip(horizontal);

            HandleAttacks();

            HandleLayers();

            ResetValues();

            
        }

    }

    public void Initialize()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        anim = GetComponentInChildren<Animator>();
        jump = false;
    }

    public void MyUpdate()
    {
        //if (myRigidBody) transform.Translate(Vector2.up * movementspeed * Time.deltaTime);
        //else transform.Translate(Vector2.down * movementspeed * Time.deltaTime);
    }

    public void DeadUpdate()
    {
        transform.Translate(Vector2.down * movementspeed * Time.deltaTime);
    }



    private void HandleMovement(float horizontal)
    {
        if(myRigidBody.velocity.y < 0 )
        {
            myAnimator.SetBool("land", true);
        }

        if (!myAnimator.GetBool("slide") && !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack")&& (isGrounded ||airControl ))
        {
            myRigidBody.velocity = new Vector2(horizontal * movementspeed, myRigidBody.velocity.y);
        }
        else if (myAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Attack"))
        {
            myRigidBody.velocity = new Vector2(0, 0);
        }

        if(isGrounded && jump)
        {
            isGrounded = false;
            myRigidBody.AddForce(new Vector2(0, jumpForce));
            myAnimator.SetTrigger("jump");
        }
               
        if (slide &&  !this.myAnimator.GetCurrentAnimatorStateInfo(0).IsName("slide"))
        {
            myAnimator.SetBool("slide",true);
        }
        else if (!this.myAnimator.GetCurrentAnimatorStateInfo(0).IsName("slide"))
        {
            myAnimator.SetBool("slide", false);
        }

        myAnimator.SetFloat("speed", Mathf.Abs(horizontal));
    }
    
    private void HandleAttacks()
    {
        if (Attack)
        {
            myAnimator.SetTrigger("Attack");
        }

    }
    private void HandleInput()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }
        
        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            Attack = true;
        }
        if(Input.GetKeyDown(KeyCode.LeftControl))
        {
            slide = true;
        }
    }
    
   

    
    private void Flip(float horizontal)
    {
        if (horizontal > 0 && !facingRight || horizontal < 0  && facingRight )
        {
            facingRight = !facingRight;

            Vector3 theScale = transform.localScale;

            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }
    private void ResetValues()
    {
        Attack = false;
        slide = false;
        jump = false;

    }
    private bool IsGrounded()
    {
        if (myRigidBody.velocity.y <= 0)
        {
            foreach (Transform point in groundPoints)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position,groundRadius,whatIsGround);

                for (int i = 0; i < colliders.Length; i++ )
                {
                    if (colliders[i].gameObject != gameObject)
                    {
                        myAnimator.ResetTrigger("jump");
                        myAnimator.SetBool("land", false);
                        return true;                      
                    }
                }
            }
        }
        return false;
    }

   private void HandleLayers()
    {
        if (!isGrounded)
        {
            myAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            myAnimator.SetLayerWeight(1, 0);
        }
    }
    void Dead()
    {
        if (isDead) return;
        isDead = true;
        anim.Play("Dead");
        gameManager.GameOver();
        SoundManager.PlaySound("Sound1");
       
    }
    
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "muerte")
        {
            Dead();
            anim.SetTrigger("dead");
        }
        if (coll.gameObject.tag == "Finish")
        {
            hud = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUD>();
            hud.OpenTitlePanel();
            

        }

    }
   

}

