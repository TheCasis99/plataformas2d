﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static AudioClip Sound1;
    static AudioSource audioSrc;

	// Use this for initialization
	void Start ()
    {
        Sound1 = Resources.Load<AudioClip>("Sound1");

        audioSrc = GetComponent<AudioSource>();
	}

    // Update is called once per frame
    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "Sound1":
                audioSrc.PlayOneShot(Sound1);
                break;
        }
       
	}
}
