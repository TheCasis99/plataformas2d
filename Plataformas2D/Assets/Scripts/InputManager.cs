﻿using UnityEngine;

public class InputManager
{
    private Player Player;
    private GameManager gameManager;

    public void Initialize()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }
    public void ReadInput()
    {
        if (gameManager.gameover) return;

        if (Input.GetButtonDown("Cancel"))
        {
            gameManager.PauseGame();
        }

        if (gameManager.isPaused) return;

        
    }
}
